package com.lda.bi.dcm.dfareporting.reports;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import com.google.api.services.dfareporting.Dfareporting;
import com.google.api.services.dfareporting.Dfareporting.Files.Get;
import com.google.api.services.dfareporting.model.File;
import com.lda.bi.dcm.dfareporting.DfaReportingFactory;
import com.lda.bi.dcm.dfareporting.main.DoubleClickReporting;

/**
 * <b>Clase para descargar los archivos correspondientes a los informes FLOODLIGTH y PATH TO CONVERSIONS
 */
public class DownloadFile {

	private Dfareporting reporting;

	/**
	 * Se inicializa el Dfareporting
	 * @throws Exception
	 */
	public DownloadFile() throws Exception{
		this.reporting = DfaReportingFactory.getInstance();
	}

	/**
	 * Metodo llamado por:<br>
	 * <ul>
	 * <li>{@link DoubleClickReporting#main(String[])}
	 * </ul>
	 * <ol>
	 * <li> Se comprueba si el archivo con id pasado como segundo parametro esta listo para descargar
	 * <li> Se descarga el archivo en formato CSV
	 * <li> Se lee el archivo recorriendo cada una de las lineas
	 * <ul>
	 * <li> Se saltan las líneas que no contienen los datos
	 * </ul>
	 * <li> Se construye una lista de tipo String con cada una de las línea que representan los datos
	 * </ol>
	 * @param reportId De tipo long
	 * @param fileId De tipo long
	 * @throws Exception
	 * @return List. List de tipo String con los datos del informe Floodlight
	 */
	public List<String> downloadReportFlood(long reportId, long fileId)
			throws Exception {
		// Retrieve the file metadata.
		File file = reporting.files().get(reportId, fileId).execute();

		List<String> datosAInsertar = new ArrayList<String>();

		if ("REPORT_AVAILABLE".equals(file.getStatus())) {
			// Prepare a local file to download the report contents to.

			//!!!!!!!!!!!TIENE QUE ESTAR CREADO EL ARCHIVO outputFlood.csv OBLIGATORIAMENTE en la carpeta src/main/resources 
			java.io.File outFile = new java.io.File(DownloadFile.class.getClassLoader().getResource("outputFlood.csv").getPath());

			// Create a get request.
			Get getRequest = reporting.files().get(reportId, fileId);      


			FileOutputStream fos = new FileOutputStream(outFile);
			getRequest.executeMediaAndDownloadTo(fos);

			//System.out.printf("File %d downloaded to %s%n", file.getId(), outFile.getAbsolutePath());

			//!!!!!!!!!!!TIENE QUE ESTAR CREADO EL ARCHIVO outputFlood.csv OBLIGATORIAMENTE en la carpeta src/main/resources
			java.io.File fileObtenido = new java.io.File(DownloadFile.class.getClassLoader().getResource("outputFlood.csv").getPath());
			BufferedReader br = new BufferedReader(new FileReader(fileObtenido));

			Stream<String> lineas = br.lines();
			Iterator<String> it = lineas.iterator();
			int i = 0;
			//20 son las lineas que se han de saltar del csv hasta llegar a los datos
			while(it.hasNext()&&i!=20){ //CUIDADO AQUI. En caso de necesitar mas ACTIVIDADES hay que modificar el 20
				//System.out.println(it.next());
				it.next();
				i++;
			}
			//Aqui se recogen los datos
			while(it.hasNext()){
				datosAInsertar.add(it.next());
				//Cuidado que tambien se obtiene la ultima linea que no me interesa
				//Suma total:,---,---,---,---,---,---,---,---,---,---,---,0.0
			}
			int size = datosAInsertar.size();
			datosAInsertar.remove(size-1);
			br.close();
		}
		return datosAInsertar;
	}

	/**
	 * Metodo llamado por:<br>
	 * <ul>
	 * <li>{@link DoubleClickReporting#main(String[])}
	 * </ul>
	 * <ol>
	 * <li> Se comprueba si el archivo con id pasado como segundo parametro esta listo para descargar
	 * <li> Se descarga el archivo en formato CSV
	 * <li> Se lee el archivo recorriendo cada una de las lineas
	 * <ul>
	 * <li> Se saltan las líneas que no contienen los datos
	 * </ul>
	 * <li> Se construye una lista de tipo String con cada una de las línea que representan los datos
	 * </ol>
	 * @param reportId De tipo long
	 * @param fileId De tipo long
	 * @throws Exception
	 * @return List. List de tipo String con los datos del informe Path To Conversions
	 */
	public List<String> downloadReportPathToConversions(long reportId, long fileId)
			throws Exception {

		List<String> datosAActualizar = new ArrayList<String>();

		// Retrieve the file metadata.
		File file = reporting.files().get(reportId, fileId).execute();
		if ("REPORT_AVAILABLE".equals(file.getStatus())) {
			// Prepare a local file to download the report contents to.
			//!!!!!!!!!!!TIENE QUE ESTAR CREADO EL ARCHIVO outputPath.csv OBLIGATORIAMENTE en la carpeta src/main/resources
			java.io.File outFile = new java.io.File(DownloadFile.class.getClassLoader().getResource("outputPath.csv").getPath());

			// Create a get request.
			Get getRequest = reporting.files().get(reportId, fileId);      

			FileOutputStream fos = new FileOutputStream(outFile);
			getRequest.executeMediaAndDownloadTo(fos);

			//System.out.printf("File %d downloaded to %s%n", file.getId(), outFile.getAbsolutePath());

			//!!!!!!!!!!!TIENE QUE ESTAR CREADO EL ARCHIVO outputPath.csv OBLIGATORIAMENTE en la carpeta src/main/resources
			java.io.File fileObtenido = new java.io.File(DownloadFile.class.getClassLoader().getResource("outputPath.csv").getPath());
			BufferedReader br = new BufferedReader(new FileReader(fileObtenido));

			Stream<String> lineas = br.lines();
			Iterator<String> it = lineas.iterator();
			int i = 0;
			//22 son las lineas que se han de saltar del csv hasta llegar a los datos
			while(it.hasNext()&&i!=22){ //CUIDADO AQUI. En caso de necesitar mas ACTIVIDADES hay que modificar el 22
				//System.out.println(it.next());
				it.next();
				i++;
			}
			//Aqui se recogen los datos
			while(it.hasNext()){
				datosAActualizar.add(it.next());
				//Cuidado que tambien se obtiene la ultima linea que no me interesa
				//Suma total:,---,---,---,---,---,---,---,---,---,---,---,0.0
			}
			int size = datosAActualizar.size();
			datosAActualizar.remove(size-1);
			br.close();
		}
		return datosAActualizar;
	}

	public List<String> downloadReport(long reportId, long fileId, int type) throws IOException{
		//type == 0 -> FLOODLIGHT
		//type == 1 -> PATH_TO_CONVERSIONS
		// Retrieve the file metadata.
		File file = reporting.files().get(reportId, fileId).execute();
		List<String> datos = new ArrayList<String>();
		if ("REPORT_AVAILABLE".equals(file.getStatus())) {
			if(type == 0){
				// Prepare a local file to download the report contents to.
				//!!!!!!!!!!!TIENE QUE ESTAR CREADO EL ARCHIVO outputFlood.csv OBLIGATORIAMENTE en la carpeta src/main/resources 
				java.io.File outFile = new java.io.File(DownloadFile.class.getClassLoader().getResource("outputFlood.csv").getPath());

				// Create a get request.
				Get getRequest = reporting.files().get(reportId, fileId);      

				FileOutputStream fos = new FileOutputStream(outFile);
				getRequest.executeMediaAndDownloadTo(fos);

				//System.out.printf("File %d downloaded to %s%n", file.getId(), outFile.getAbsolutePath());

				//!!!!!!!!!!!TIENE QUE ESTAR CREADO EL ARCHIVO outputFlood.csv OBLIGATORIAMENTE en la carpeta src/main/resources
				java.io.File fileObtenido = new java.io.File(DownloadFile.class.getClassLoader().getResource("outputFlood.csv").getPath());
				BufferedReader br = new BufferedReader(new FileReader(fileObtenido));

				Stream<String> lineas = br.lines();
				Iterator<String> it = lineas.iterator();
				int i = 0;
				//20 son las lineas que se han de saltar del csv hasta llegar a los datos
				while(it.hasNext()&&i!=20){ //CUIDADO AQUI. En caso de necesitar mas ACTIVIDADES hay que modificar el 20
					//System.out.println(it.next());
					it.next();
					i++;
				}
				//Aqui se recogen los datos
				while(it.hasNext()){
					datos.add(it.next());
					//Cuidado que tambien se obtiene la ultima linea que no me interesa
					//Suma total:,---,---,---,---,---,---,---,---,---,---,---,0.0
				}
				int size = datos.size();
				datos.remove(size-1);
				br.close();
			}
			if(type == 1){
				// Prepare a local file to download the report contents to.
				//!!!!!!!!!!!TIENE QUE ESTAR CREADO EL ARCHIVO outputPath.csv OBLIGATORIAMENTE en la carpeta src/main/resources
				java.io.File outFile = new java.io.File(DownloadFile.class.getClassLoader().getResource("outputPath.csv").getPath());

				// Create a get request.
				Get getRequest = reporting.files().get(reportId, fileId);      

				FileOutputStream fos = new FileOutputStream(outFile);
				getRequest.executeMediaAndDownloadTo(fos);

				//System.out.printf("File %d downloaded to %s%n", file.getId(), outFile.getAbsolutePath());

				//!!!!!!!!!!!TIENE QUE ESTAR CREADO EL ARCHIVO outputPath.csv OBLIGATORIAMENTE en la carpeta src/main/resources
				java.io.File fileObtenido = new java.io.File(DownloadFile.class.getClassLoader().getResource("outputPath.csv").getPath());
				BufferedReader br = new BufferedReader(new FileReader(fileObtenido));

				Stream<String> lineas = br.lines();
				Iterator<String> it = lineas.iterator();
				int i = 0;
				//22 son las lineas que se han de saltar del csv hasta llegar a los datos
				while(it.hasNext()&&i!=22){ //CUIDADO AQUI. En caso de necesitar mas ACTIVIDADES hay que modificar el 22
					//System.out.println(it.next());
					it.next();
					i++;
				}
				//Aqui se recogen los datos
				while(it.hasNext()){
					datos.add(it.next());
					//Cuidado que tambien se obtiene la ultima linea que no me interesa
					//Suma total:,---,---,---,---,---,---,---,---,---,---,---,0.0
				}
				int size = datos.size();
				datos.remove(size-1);
				br.close();
			}
		}
		return datos;
	}
}