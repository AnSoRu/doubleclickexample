package com.lda.bi.dcm.dfareporting.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import com.lda.seguridad.encriptacion.EncriptaClave;

public class DoubleClickDAO {

	private static final String DOUBLE_CLICK_SOURCE = "DDS_OWN.DW_MK_TF_DOUBLECLICK";
	

	private String conexion;

	public DoubleClickDAO(int entorno){
		//Entorno desarrollo
		if(entorno==1){
			this.conexion = "jdbc:oracle:thin:@(DESCRIPTION_LIST=(FAILOVER=ON)(LOAD_BALANCE=OFF)(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SD0SIG)))(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SD0SIG))))";

		}//Entorno training
		if(entorno==2){
			this.conexion = "jdbc:oracle:thin:@(DESCRIPTION_LIST=(FAILOVER=ON)(LOAD_BALANCE=OFF)(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan1.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan2.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan3.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=ST0SIG)))(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan1.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan2.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan3.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=ST0SIG))))";

		}//Entorno produccion
		if(entorno==3){
			this.conexion = "jdbc:oracle:thin:@(DESCRIPTION_LIST=(FAILOVER=ON)(LOAD_BALANCE=OFF)(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan1.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan2.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan3.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SP0SIG)))(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan1.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan2.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan3.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SP0SIG))))";
		}
	}

	public void saveDoubleClickSources(List<String> data) throws ClassNotFoundException, SQLException, ParseException{

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection = null;
		
		connection = DriverManager.getConnection(conexion,"big_data",EncriptaClave.desencripta("109A48A-69A114A-86A127A108A87A"));

		PreparedStatement ps = null;

		String cod_cot_exception = "";

		try{
			connection.setAutoCommit(false);

			ps = connection.prepareCall("INSERT INTO " + DOUBLE_CLICK_SOURCE + "(COD_COT, FEC_COT, FEC_VISITA,"
					+ "DES_SITIO_WEB, DES_CAMPANA, DES_DOMINIO_RED,"
					+ "DES_ENLACE, DES_PISTA_CREATIVA, DES_TIPO_CONVERSION, DES_POST_IMPRESION,"
					+ "DES_ORIGEN, DES_CUENTA, DES_PALABRA_CLAVE) "
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");

			for (String datum: data){
				//System.out.println(datum);
				String [] aux = datum.split(",");
				if (aux.length < 13) {
					continue;
				}
				cod_cot_exception = aux[0];
				
				ps.setString(1,aux[0]); //cod_cot 
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				ps.setTimestamp(2, 
						new java.sql.Timestamp(((java.util.Date) 
								sdf.parse(aux[1])).getTime())); // fec_cot
				
				if(!aux[2].equals("X0")){
					ps.setTimestamp(3, 
							new java.sql.Timestamp(((java.util.Date) 
									sdf.parse(aux[2])).getTime()));
				} // fec_visita
				else{
					ps.setTimestamp(3, new java.sql.Timestamp(0));
				}
				ps.setString(4,aux[3]); //des_sitio_web
				ps.setString(5,aux[4]); //des_campana
				ps.setString(6,aux[5]); //des_dominio_red
				ps.setString(7,aux[6]); //des_enlace
				ps.setString(8,aux[7]); //des_pista_creativa
				ps.setString(9,aux[8]); //des_tipo_conversion
				ps.setString(10,aux[12]); //des_post_impresion ES LA METRICA
				ps.setString(11,aux[9]); //des_origen
				ps.setString(12,aux[10]); //des_cuenta
				ps.setString(13,aux[11]); //des_palabra_clave
				ps.addBatch();
			}
			ps.executeBatch();
			connection.commit();
		}catch (SQLException e) {
			System.out.println(cod_cot_exception);
			throw new RuntimeException(e);
		} finally {
			this.connectionCleanup(ps, connection);
		}
	}

	private boolean connectionCleanup(Statement st, Connection conn) {
		try {
			if (st != null)
				st.close();
			if (conn != null)
				conn.close();
			return true;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}