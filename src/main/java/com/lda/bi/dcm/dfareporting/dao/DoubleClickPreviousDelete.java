package com.lda.bi.dcm.dfareporting.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.log4j.Logger;

import com.lda.seguridad.encriptacion.EncriptaClave;

public class DoubleClickPreviousDelete {

	private static final Logger logger = Logger.getLogger(DoubleClickPreviousDelete.class.getName());
	private static final String DOUBLE_CLICK_SOURCE = "DDS_OWN.DW_MK_TF_DOUBLECLICK";

	private String conexion;

	public DoubleClickPreviousDelete(int entorno){
		//Entorno desarrollo
		if(entorno==1){
			this.conexion = "jdbc:oracle:thin:@(DESCRIPTION_LIST=(FAILOVER=ON)(LOAD_BALANCE=OFF)(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SD0SIG)))(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SD0SIG))))";

		}//Entorno training
		if(entorno==2){
			this.conexion = "jdbc:oracle:thin:@(DESCRIPTION_LIST=(FAILOVER=ON)(LOAD_BALANCE=OFF)(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan1.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan2.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan3.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=ST0SIG)))(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan1.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan2.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan3.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=ST0SIG))))";

		}//Entorno produccion
		if(entorno==3){
			this.conexion = "jdbc:oracle:thin:@(DESCRIPTION_LIST=(FAILOVER=ON)(LOAD_BALANCE=OFF)(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan1.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan2.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa02-scan3.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SP0SIG)))(DESCRIPTION=(RETRY_COUNT=3)(CONNECT_TIMEOUT=5)(TRANSPORT_CONNECT_TIMEOUT=3)(ADDRESS_LIST=(LOAD_BALANCE=ON)(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan1.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan2.lda)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=exa01-scan3.lda)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SP0SIG))))";
		}
	}


	// Se cierra la conexión a Oracle.
	private boolean connectionCleanup(Statement st, Connection conn) {
		try {
			if (st != null)
				st.close();
			if (conn != null)
				conn.close();
			return true;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}


	public void deleteFromDate(Date dateInit) throws ClassNotFoundException, SQLException {

		String table = DOUBLE_CLICK_SOURCE;

		Connection connection = null;
		PreparedStatement ps = null;

		Class.forName("oracle.jdbc.driver.OracleDriver");
		connection = DriverManager.getConnection(conexion,"big_data",EncriptaClave.desencripta("109A48A-69A114A-86A127A108A87A"));

		try {

			logger.info("Se van a borrar de la tabla " + table + " los datos cuya FEC_COT sea == " + dateInit.toString());
			ps = connection.prepareCall("DELETE FROM " + table + " WHERE TRUNC(FEC_COT) = ?");
		
			ps.setDate(1,new java.sql.Date(dateInit.getTime()));
			
			ps.addBatch();
			
			ps.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}  finally {
			this.connectionCleanup(ps, connection);
		}
	}

	/*public static void main(String [] args){
		//Borrar datos previos
		DoubleClickPreviousDelete dcpd = new DoubleClickPreviousDelete(1);
		Date today = new Date();
		try {
			dcpd.deleteFromDate(new DateTime(today).minusDays(1).toDate());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

}
