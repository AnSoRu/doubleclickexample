package com.lda.bi.dcm.dfareporting.reports;

import java.util.ArrayList;
import java.util.List;

import com.google.api.services.dfareporting.Dfareporting;
import com.google.api.services.dfareporting.model.DateRange;
import com.google.api.services.dfareporting.model.DimensionValue;
import com.google.api.services.dfareporting.model.Report;
import com.google.api.services.dfareporting.model.Report.FloodlightCriteria;
import com.google.api.services.dfareporting.model.Report.FloodlightCriteria.ReportProperties;
import com.google.api.services.dfareporting.model.SortedDimension;
import com.google.common.collect.ImmutableList;
import com.lda.bi.dcm.dfareporting.DfaReportingFactory;
import com.lda.seguridad.encriptacion.EncriptaClave;

public class CreateReportFloodlight {

	private static final String REPORT_NAME = "Floodlight_Report_Batch_BI_NO_ELIMINAR_NO_MODIFICAR";

	private Dfareporting reporting;

	private long profileId;


	public CreateReportFloodlight() throws Exception {
		this.reporting = DfaReportingFactory.getInstance();
		this.profileId = Long.parseLong(EncriptaClave.desencripta("126A49A-105A89A44A48A74A-33A"));
	}

	public long createReport() throws Exception {
		// Create a date range to report on.
		DateRange dateRange = new DateRange();
		// dateRange.setStartDate(new DateTime(fechaInicio));
		// dateRange.setEndDate(new DateTime(fechaFin));
		dateRange.setRelativeDateRange("YESTERDAY");

		// Create a dimension to report on.
		SortedDimension dfaordValue = new SortedDimension();
		dfaordValue.setName("dfa:ordValue");

		SortedDimension dfaActivityTime = new SortedDimension();
		dfaActivityTime.setName("dfa:activityTime");

		SortedDimension dfaAInteractionTime = new SortedDimension();
		dfaAInteractionTime.setName("dfa:interactionTime");

		SortedDimension dfa31 = new SortedDimension();
		dfa31.setName("dfa:floodlightVariableDimension31");

		SortedDimension dfaCampaign = new SortedDimension();
		dfaCampaign.setName("dfa:campaign");

		SortedDimension dfaSite = new SortedDimension();
		dfaSite.setName("dfa:site");

		SortedDimension dfaPlacement = new SortedDimension();
		dfaPlacement.setName("dfa:placement");

		SortedDimension dfaCreative = new SortedDimension();
		dfaCreative.setName("dfa:creative");

		SortedDimension dfaActivity = new SortedDimension();
		dfaActivity.setName("dfa:activity");

		SortedDimension dfaPSC = new SortedDimension();
		dfaPSC.setName("dfa:paidSearchCampaign");

		SortedDimension dfaPSEA = new SortedDimension();
		dfaPSEA.setName("dfa:paidSearchEngineAccount"); // SEARCH

		SortedDimension dfaPSK = new SortedDimension();
		dfaPSK.setName("dfa:paidSearchKeyword");

		DimensionValue dfaFLCId = new DimensionValue();
		dfaFLCId.setDimensionName("dfa:floodlightConfigId");
		dfaFLCId.setValue("6494367");

		List<SortedDimension> dimensionList = new ArrayList<SortedDimension>();
		dimensionList.add(dfaordValue);// 0 -> COD_COT
		dimensionList.add(dfaActivityTime);// 1 -> FEC_COT
		dimensionList.add(dfaAInteractionTime); // 2 -> FEC_VISITA ??? COMPROBAR
		dimensionList.add(dfa31);// 3 -> DES_SITIO_WEB
		dimensionList.add(dfaCampaign);// 4 -> DES_CAMPANA
		dimensionList.add(dfaSite);// 5 -> DES_DOMINIO_RED
		dimensionList.add(dfaPlacement);// 6 -> DES_ENLACE
		dimensionList.add(dfaCreative);// 7 -> DES_PISTA_CREATIVA
		dimensionList.add(dfaActivity);// 8 -> DES_TIPO_CONVERSION
		dimensionList.add(dfaPSC);// 9 -> DES_ORIGEN
		dimensionList.add(dfaPSEA);// 10 -> DES_CUENTA
		dimensionList.add(dfaPSK);// 11 -> DES_PALABRA_CLAVE


		FloodlightCriteria flc = new FloodlightCriteria();
		flc.setDateRange(dateRange);
		flc.setDimensions(dimensionList);
		flc.setFloodlightConfigId(dfaFLCId);
		flc.setMetricNames(ImmutableList.of("dfa:activityViewThroughConversions")); // POST_IMPRESION

		ReportProperties rp = new ReportProperties();
		rp.setIncludeUnattributedCookieConversions(true);
		rp.setIncludeUnattributedIPConversions(true);


		flc.setReportProperties(rp);


		Report report = new Report();
		report.setFloodlightCriteria(flc);

		//El problema es que un Floodlight report no admite un Criteria, tiene que ser obligatoriamente un FloodlightCriteria
		//report.getCriteria().getActivities(); //-> Esto no lo admite el floodlight

		//report.setUnknownKeys(activities);
		report.setName(REPORT_NAME);
		report.setType("FLOODLIGHT");

		// Insert the report.
		Report result = reporting.reports().insert(profileId, report).execute();

		// Display the new report ID.
		System.out.printf("Floodlight report with ID %d was created.%n", result.getId());

		return result.getId();
	}

	/**
	 * <b>Si se diera el caso de que se eliminasen los reports del batch en DCM<br></b>
	 * Realizar en orden los siguientes pasos:<br>
	 * <ol>
	 * <li>Descomentar el main de esta clase {@link CreateReportFloodlight} y ejecutar
	 * <li>Anotar el id del report devuelto por DCM
	 * <li>Ir a DCM
	 * <li>En la vista de informes seleccionar aquel con el id devuelto
	 * <li><b>Seleccionar las actividades floodlight sobre las que crear el report</b>
	 * <ul>
	 * <li><b>NOTA:</b> asegurarse de seleccionar las <b>MISMAS</b> actividades de Floodlight en ambos reports, el creado por la clase {@link CreateReportFloodlight} y el creado por la clase {@link CreateReportPathToConversions}
	 * <li>A fecha de 13-08-2018 son 8 las actividades seleccionadas
	 * </ul>
	 * <li>Pulsar en "Guardar".<b> No pulsar sobre "Guardar y Generar"</b>
	 * <li>Ir a la clase {@link CreateReportPathToConversions} y ejecutar
	 * <li>Ir al metodo {@link DoubleClickReporting#main(String[])} y cambiar el <b>valor</b> del parametro que se le pasa al objeto {@link RunReport} (de Floodlight) por el ID del report devuelto
	 * </ol>
	 * @author unkasr
	 */
	/*public static void main(String[] args) throws Exception { 
		CreateReportFloodlight crf = new CreateReportFloodlight();
		System.out.println(crf.createReport());
	}*/
}