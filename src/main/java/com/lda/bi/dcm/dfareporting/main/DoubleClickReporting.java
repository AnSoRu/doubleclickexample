package com.lda.bi.dcm.dfareporting.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;

import com.lda.bi.dcm.dfareporting.dao.DoubleClickDAO;
import com.lda.bi.dcm.dfareporting.dao.DoubleClickPreviousDelete;
import com.lda.bi.dcm.dfareporting.reports.CreateReportFloodlight;
import com.lda.bi.dcm.dfareporting.reports.CreateReportPathToConversions;
import com.lda.bi.dcm.dfareporting.reports.DownloadFile;
import com.lda.bi.dcm.dfareporting.reports.RunReport;
import com.lda.seguridad.encriptacion.EncriptaClave;

public class DoubleClickReporting {

	/**
	 * Metodo para cambiar el formato a los datos de Floodlight<br>
	 * Metodo llamado por:<br>
	 * <ul>
	 * <li>{@link DoubleClickReporting#main(String[])}
	 * </ul>
	 * <ol>
	 * <li>Se splita cada una de las cadenas recibidas por parametro
	 * <li>Se comprueba si la longitud del codigo de cotizacion es mayor que 8
	 * <ul>
	 * <li>Si es mayor que 8<br>
	 * <ul>
	 * <li>Se extraen los 7 primeros caracteres y se añade una X al comienzo
	 * </ul>
	 * </ul>
	 * <li>Se forma una cadena compuesta por los 11 elementos recibidos
	 * <li>Se añade a una lista final de tipo String
	 * </ol>
	 * @param floodList Lista de tipo String
	 * @return List. Lista de tipo String con los datos del informe Floodlight
	 */
	public List<String> prepareFloodList(List<String> floodList){

		List<String> finalFloodList = new ArrayList<String>();

		for(String datum: floodList){
			String [] aux = datum.split(",");
			if(aux.length < 12){
				continue;
			}
			if(aux[0].length() > 8){
				aux[0] = "X" + aux[0].substring(0,7);
			}
			String result = aux[0] + "," +
					aux[1] + "," +
					aux[2] + "," +
					aux[3] + "," +
					aux[4] + "," + 
					aux[5] + "," +
					aux[6] + "," +
					aux[7] + "," +
					aux[8] + "," +
					aux[9] + "," +
					aux[10] + "," +
					aux[11];
			finalFloodList.add(result);
		}
		return finalFloodList;
	}

	/**
	 * Metodo para cambiar el formato a los datos de Floodlight<br>
	 * Metodo llamado por:<br>
	 * <ul>
	 * <li>{@link DoubleClickReporting#main(String[])}
	 * </ul>
	 * <ol>
	 * <li>Se splita cada una de las cadenas recibidas por parametro
	 * <li>Se comprueba si la longitud del codigo de cotizacion es mayor que 8
	 * <ul>
	 * <li>Si es mayor que 8<br>
	 * <ul>
	 * <li>Se extraen los 7 primeros caracteres y se añade una X al comienzo
	 * </ul>
	 * </ul>
	 * <li>Se forma una cadena compuesta por los 4 elementos recibidos
	 * <li>Se añade a una lista final de tipo String
	 * </ol>
	 * @param pathList Lista de tipo String
	 * @return List. Lista de tipo String con los datos del informe Path To Conversions
	 */
	public List<String> preparePathList(List<String> pathList){

		List<String> finalPathList = new ArrayList<String>();

		for(String datum: pathList){
			String [] aux = datum.split(",");
			if(aux.length < 5){
				continue;
			}
			if(aux[0].length() > 8){
				aux[0] = "X" + aux[0].substring(0,7);
			}
			String result = aux[0] + "," +
					aux[1] + "," +
					aux[2] + "," +
					aux[3] + "," +
					aux[4];
			finalPathList.add(result);
		}
		return finalPathList;
	}
	
	/**
	 * Metodo llamado por:<br>
	 * <ul>
	 * <li>{@link DoubleClickReporting#mergeLists(List, List)}
	 * </ul>
	 * @param data Lista de String
	 * @return String. Lista recibida como paremtro convertida a una cadena separada por comas
	 */
	private String stringFromList(List<String> data){
		String result = data.get(0) + ",";
		for(int i = 1; i < data.size() - 1; i++){
			result = result + data.get(i) + ",";
		}
		result = result + data.get(data.size()-1);
		return result;
	}

	/**
	 * Metodo llamado por:<br>
	 * <ul>
	 * <li>{@link DoubleClickReporting#main(String[])}
	 * </ul>
	 * @param floodData List de tipo String con los datos del informe Floodlight
	 * @param pathData List de tipo String con los datos del informe Path To Conversions
	 * @return List. Lista de tipo String con los datos cruzados
	 */
	public List<String> mergeLists(List<String> floodData, List<String> pathData){
		//Si en pathToConversions hay una cod_cod que este en floodData
		//Coger el campo 2 (fec_visita)
		HashMap<String,List<String>> floodDataHash = new HashMap<String, List<String>>();
		if((!floodData.isEmpty()) && (!pathData.isEmpty())){
			for(String datumFlood: floodData){
				String [] aux = datumFlood.split(",");
				List<String> lAux = new ArrayList<String>();
				int j = 0;
				for(int i = 0; i < aux.length; i++){ //Esta lista tiene 12 elementos
					if(j == 2){
						lAux.add(j,"X0");
						j++;
					}
					lAux.add(j, aux[i]); //Esta lista deberia de tener 13 elementos
					j++;
				}
				floodDataHash.put(aux[0], lAux); //lAux contiene 13 elementos
			}
			for(String datumPath: pathData){
				String [] aux = datumPath.split(",");
				if(floodDataHash.containsKey(aux[0])){ //Modifico el X0
					if(!aux[2].isEmpty()){ //Esto debería ser empty
						List<String> floodData2 = floodDataHash.get(aux[0]);
						floodData2.remove(2);
						floodData2.add(2, aux[2]);
						floodDataHash.put(aux[0], floodData2); //Modifico 
					}
				}
			}
		}
		//Recupero las claves de floodDataHash
		Set<String> claves = floodDataHash.keySet();
		List<String> result = new ArrayList<String>();
		for(String s: claves){
			List<String> lista = floodDataHash.get(s);
			String sAux = stringFromList(lista);
			result.add(sAux);
		}
		return result;
	}

	/**<b>NOTA PREVIA:<br></b>
	 * Deben de existir en DCM un report de tipo Floodlight y un report Path To Conversions<br>
	 * previamente creados.<br>
	 * <b>SI NO EXISTEN EJECUTAR antes de esta clase {@link DoubleClickReporting} los main de las dos siguientes:</b><br>
	 * <ul>
	 * <li>{@link CreateReportFloodlight#main(String[])} (Descomentar previamente)
	 * <li>{@link CreateReportPathToConversions#main(String[])} (Descomentar previamente)
	 * </ul>
	 * <ol>
	 * <li>Se recibe el entorno (D,T,P) como primer parametro
	 * <li>Se manda ejecutar el informe de tipo Floodlight
	 * <li>Se manda ejecutar el informe de tipo Path To Conversions
	 * <li>Se cruzan lo datos de ambos informes en una única lista
	 * <li>Se almacenan en la tabla correspondiente
	 * </ol>
	 * @param args args[0] es el entorno de ejecucion
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		if(args.length!=1){
			System.err.println("Número de parámetros incorrecto");
			System.exit(-1);	
		}

		/*String startDate = args[0];
		String endDate = args[1];*/
		String entorno = args[0];

		int entornoNumero = -1;

		if(entorno.toLowerCase().equals("d")){
			entornoNumero = 1;
		}
		else if(entorno.toLowerCase().equals("t")){
			entornoNumero = 2;
		}
		else if(entorno.toLowerCase().equals("p")){
			entornoNumero = 3;
		}else{
			System.err.println("Parametro de entorno no valido");
			System.err.println("Introducir 'D o d' para desarrollo");
			System.err.println("Introducir 'T o t' para training");
			System.err.println("Introducir 'P o p' para produccion");
			System.exit(-1);
		}

		/*String startDateFormateada = startDate.substring(0, 4) + "-" + startDate.substring(4,6) + "-" + startDate.substring(6,8);
		String endDateFormateada = endDate.substring(0, 4) + "-" + endDate.substring(4,6) + "-" + endDate.substring(6,8);

		String [] fechas = new String[2];

		fechas[0] = startDateFormateada;
		fechas[1] = endDateFormateada;*/

		DoubleClickReporting dcr = new DoubleClickReporting();

		//FLOODLIGHT
		//RunReportFloodlight

		RunReport rRepFlood = new RunReport(Long.valueOf(EncriptaClave.desencripta("52A-27A120A104A-93A-49A-37A-47A79A70A23A76A0A23A72A89A")));
		long fileIdFlood = rRepFlood.runReport();
		if(fileIdFlood == -1){
			System.exit(-1);
		}
		if(fileIdFlood == -2){
			System.exit(-2);
		}

		//Download Floodlight Reports	
		DownloadFile df = new DownloadFile();
		//List<String> floodReports = dfFloodAndPath.downloadReportFlood(Long.valueOf(EncriptaClave.desencripta("52A-27A120A104A-93A-49A-37A-47A79A70A23A76A0A23A72A89A")), fileIdFlood);
		List<String> floodReports = df.downloadReport(Long.valueOf(EncriptaClave.desencripta("52A-27A120A104A-93A-49A-37A-47A79A70A23A76A0A23A72A89A")), fileIdFlood, 0);
		List<String> preparedFloodList = dcr.prepareFloodList(floodReports);

		//-----------------------------
		//PATH_TO_CONVERSIONS
		//RunReportPathToConversions

		RunReport rRepPath = new RunReport(Long.valueOf(EncriptaClave.desencripta("-11A32A-6A-59A-71A52A-121A-66A19A-8A127A110A79A12A26A-9A")));
		long fileIdPath = rRepPath.runReport();
		if(fileIdPath == -1){
			System.exit(-1);
		}
		if(fileIdPath == -2){
			System.exit(-2);
		}

		//Download Path to Conversions Reports
		//List<String> pathToConversionReports = dfFloodAndPath.downloadReportPathToConversions(Long.valueOf(EncriptaClave.desencripta("-11A32A-6A-59A-71A52A-121A-66A19A-8A127A110A79A12A26A-9A")),fileIdPath);
		List<String> pathToConversionReports = df.downloadReport(Long.valueOf(EncriptaClave.desencripta("-11A32A-6A-59A-71A52A-121A-66A19A-8A127A110A79A12A26A-9A")), fileIdPath, 1);
		List<String> preparedPathList = dcr.preparePathList(pathToConversionReports);

		//Aqui hay que construir cada string de la lista a insertar

		List<String> finalList = dcr.mergeLists(preparedFloodList, preparedPathList);
		
		//Borrar datos previos
		DoubleClickPreviousDelete dcpd = new DoubleClickPreviousDelete(entornoNumero);
		Date today = new Date();
		dcpd.deleteFromDate(new DateTime(today).minusDays(1).toDate());

		//Insertar la lista final

		DoubleClickDAO dcDao = new DoubleClickDAO(entornoNumero);
		dcDao.saveDoubleClickSources(finalList);
	}
}