package com.lda.bi.dcm.dfareporting.reports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.api.services.dfareporting.Dfareporting;
import com.google.api.services.dfareporting.model.DateRange;
import com.google.api.services.dfareporting.model.DimensionValue;
import com.google.api.services.dfareporting.model.Report;
import com.google.api.services.dfareporting.model.Report.PathToConversionCriteria;
import com.google.api.services.dfareporting.model.Report.PathToConversionCriteria.ReportProperties;
import com.google.api.services.dfareporting.model.SortedDimension;
import com.google.common.collect.ImmutableList;
import com.lda.bi.dcm.dfareporting.DfaReportingFactory;
import com.lda.seguridad.encriptacion.EncriptaClave;

public class CreateReportPathToConversions {

	private static final String REPORT_NAME = "Path_To_Conversions_Report_Batch_BI__NO_ELIMINAR_NO_MODIFICAR";

	private Dfareporting reporting;

	private long profileId;

	
	public CreateReportPathToConversions() throws Exception {
		this.reporting = DfaReportingFactory.getInstance();
		this.profileId = Long.parseLong(EncriptaClave.desencripta("126A49A-105A89A44A48A74A-33A"));
	}

	public long createReport() throws IOException{

		DateRange dateRange = new DateRange();
		//dateRange.setStartDate(new DateTime(fechaInicio));
		//dateRange.setEndDate(new DateTime(fechaFin));
		dateRange.setRelativeDateRange("YESTERDAY");

		// Create a dimension to report on.
		SortedDimension dfaconversionId = new SortedDimension();
		dfaconversionId.setName("dfa:conversionId");	
		
		SortedDimension dfaordValue = new SortedDimension();
		dfaordValue.setName("dfa:ordValue");

		SortedDimension dfaInteractionTime = new SortedDimension();
		dfaInteractionTime.setName("dfa:interactionTime");
		
		SortedDimension dfaInteractionNumber = new SortedDimension();
		dfaInteractionNumber.setName("dfa:interactionNumber");

		List<SortedDimension> dimensionList = new ArrayList<SortedDimension>();
		dimensionList.add(dfaordValue);
		dimensionList.add(dfaconversionId);
		
		List<SortedDimension> interactionDimensionList = new ArrayList<SortedDimension>();
		interactionDimensionList.add(dfaInteractionTime);
		interactionDimensionList.add(dfaInteractionNumber);

		ReportProperties rp = new ReportProperties();
		rp.setIncludeUnattributedCookieConversions(true);
		
		DimensionValue dfaFLCId = new DimensionValue();
		dfaFLCId.setDimensionName("dfa:floodlightConfigId");
		dfaFLCId.setValue("6494367");

		PathToConversionCriteria ptcc = new PathToConversionCriteria();
		ptcc.setDateRange(dateRange);
		ptcc.setConversionDimensions(dimensionList);
		ptcc.setPerInteractionDimensions(interactionDimensionList);
		ptcc.setReportProperties(rp);
		ptcc.setFloodlightConfigId(dfaFLCId);
		ptcc.setMetricNames(ImmutableList.of("dfa:activityViewThroughConversions"));

		Report report = new Report();
		report.setPathToConversionCriteria(ptcc);
	
		report.setName(REPORT_NAME);
		report.setType("PATH_TO_CONVERSION");

		// Insert the report.
		Report result = reporting.reports().insert(profileId, report).execute();

		// Display the new report ID.
		System.out.printf("Path To Conversion report with ID %d was created.%n", result.getId());

		return result.getId();
	}
	
	/**
	 * <b>Si se diera el caso de que se eliminasen los reports del batch en DCM<br></b>
	 * Realizar en orden los siguientes pasos:<br>
	 * <ol>
	 * <li>Descomentar el main de esta clase {@link CreateReportPathToConversions} y ejecutar
	 * <li>Anotar el id del report devuelto por DCM
	 * <li>Ir a DCM
	 * <li>En la vista de informes seleccionar aquel con el id devuelto
	 * <li><b>Seleccionar las actividades floodlight sobre las que crear el report</b>
	 * <ul>
	 * <li><b>NOTA:</b> asegurarse de seleccionar las <b>MISMAS</b> actividades de Floodlight en ambos reports, el creado por la clase {@link CreateReportFloodlight} y el creado por la clase {@link CreateReportPathToConversions}
	 * <li>A fecha de 13-08-2018 son 8 las actividades seleccionadas
	 * </ul>
	 * <li>Pulsar en "Guardar".<b> No pulsar sobre "Guardar y Generar"</b>
	 * <li><b>SOLO SI NO SE HA EJECUTADO</b> Ir a la clase {@link CreateReportFloodlight} y ejecutar
	 * <li>Ir al metodo {@link DoubleClickReporting#main(String[])} y cambiar el <b>valor</b> del parametro que se le pasa al objeto {@link RunReport} (de Path To Conversions) por el ID del report devuelto
	 * </ol>
	 * @author unkasr
	 */
	/*public static void main(String[] args) throws Exception { 
		CreateReportPathToConversions crptc = new CreateReportPathToConversions();
		System.out.println(crptc.createReport());
	}*/
}