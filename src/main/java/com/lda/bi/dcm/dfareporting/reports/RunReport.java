// Copyright 2014 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.lda.bi.dcm.dfareporting.reports;

import com.google.api.client.util.BackOff;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.dfareporting.Dfareporting;
import com.google.api.services.dfareporting.model.File;
import com.lda.bi.dcm.dfareporting.DfaReportingFactory;
import com.lda.bi.dcm.dfareporting.main.DoubleClickReporting;
import com.lda.seguridad.encriptacion.EncriptaClave;


public class RunReport {
    
  private Dfareporting reporting;
  
  private long profileId;
  private long reportId;
  
  public RunReport(long reportId) throws Exception{
	  this.reporting = DfaReportingFactory.getInstance();
	  this.profileId = Long.parseLong(EncriptaClave.desencripta("126A49A-105A89A44A48A74A-33A"));
	  this.reportId = reportId;
  }

  /**
   * Metodo llamado por:<br>
   * <ul>
   * <li>{@link DoubleClickReporting#main(String[])}
   * </ul>
   * <ol>
   * <li>Se manda ejecutar el informe con id recibido en el constructor
   * <li>Se realizan peticiones a Google para comprobar si el informe esta generado
   * <li>El proceso se duerme de forma incremental durante un tiempo específico
   * <li>El proceso se despierta pasado el tiempo
   * <li>Se comprueba el estado del archivo
   * <ul>
   * <li>Si devuelve un -1 significa que el informe ha fallado
   * <li>Si devuelve un -2 significa que no se vuelve a realizar mas peticiones
   * <li>En otro caso se devuelve el id del archivo generado para el informe
   * </ul>
   * </ol>
   * @return long. Dependiendo de las condiciones explicadas
   * @throws Exception
   */
  public long runReport() throws Exception {
	  
    // Run the report.
    File file = reporting.reports().run(profileId, reportId).execute();
    
    //System.out.printf("File with ID %d has been created.%n", file.getId());

    // Wait for the report file to finish processing.
    // An exponential backoff policy is used to limit retries and conserve request quota.
    BackOff backOff =
        new ExponentialBackOff.Builder()
            .setInitialIntervalMillis(10 * 1000) // 10 second initial retry
            //.setMaxIntervalMillis(10 * 60 * 1000) // 10 minute maximum retry
            .setMaxIntervalMillis(1* 60 * 1000) // 1 minute maximum retry
            .setMaxElapsedTimeMillis(120 * 60 * 1000) // 1 hour total retry
            .build();

    do {
      file = reporting.files().get(file.getReportId(), file.getId()).execute();

      if ("REPORT_AVAILABLE".equals(file.getStatus())) {
        // File has finished processing.
        //System.out.printf("File status is %s, ready to download.%n", file.getStatus());
        return file.getId();
        
      } else if (!"PROCESSING".equals(file.getStatus())) {
        // File failed to process.
        //System.out.printf("File status is %s, processing failed.", file.getStatus());
        return -1;
      }

      // The file hasn't finished processing yet, wait before checking again.
      long retryInterval = backOff.nextBackOffMillis();
      if (retryInterval == BackOff.STOP) {
        //System.out.println("File processing deadline exceeded.%n");
        return -2;
      }

      //System.out.printf("File status is %s, sleeping for %dms.%n", file.getStatus(), retryInterval);
      Thread.sleep(retryInterval);
    } while (true);
  }
}